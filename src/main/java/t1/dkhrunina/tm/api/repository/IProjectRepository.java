package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    int getSize();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}