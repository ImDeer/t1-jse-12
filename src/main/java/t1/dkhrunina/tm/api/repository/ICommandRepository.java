package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}